# Trabajos Pràctico De Programación GIT

### Alumno: Joaquin Zuin
### Carrera: Programaciòn 3
### email: joaquinzuin16@hotmail.com


## 1. ¿Qué es git?

  Git es un sistema de control de versiones.Un sistema de control de versiones nos va a servir para trabajar en equipo de una manera mucho más simple y optima cuando estamos desarrollando software.

  Se define como control de versiones a la gestión de los diversos cambios que se realizan sobre los elementos de algún producto o una configuración del mismo es decir a la gestión de los diversos cambios que se realizan sobre los elementos de algún producto o una configuración, y para los que aún no les queda claro del todo, control de versiones es lo que se hace al momento de estar desarrollando un software o una página web

## 2. ¿Por qué queremos utilizar git?
   
  Con Git se va a poder controlar todos los cambios que se hacen en nuestra aplicación y en nuestro código y vamos a tener control absoluto de todo lo que pasa en el código, pudiendo volver atrás en el tiempo, pudiendo abrir diferentes ramas de desarrollo.
  
  Vamos a poder trabajar en equipo de una manera muy sencilla y optimizada, de forma que si tenemos dos o tres personas trabajando en ciertas funcionalidades del proyecto y nosotros podemos estar trabajando en nuestra parte del código. Cuando acabamos de desarrollar nuestro código, utilizamos Git para mezclar los cambios con los otros compañeros. De forma que el código se mezcla de manera perfecta sin generar ningún tipo de fallo y de forma rápida.


## 3. ¿Qué es el bash que instala git?

   El git bash es una herramienta de tipo consola que básicamente  permite manipular y gestionar todo el proceso a realizar con el proyecto.

## 4. Describa los estados (working directory, staging area, repository)

![](/imagen/Imagen4.png)

### Working Directory:

  ES donde podemos hacer cualquier cambio y no afectar nuestro repositorio en lo absoluto.

  En cuanto modificamos algo de nuestro código, éste tiene status de modificado.

  Si ejecutamos el comando git status, nos mostrará qué archivos han sido modificados (o creados).

![](/imagen/Imagen6.png)

  Una vez que hemos hecho los cambios necesarios, pasamos nuestros archivos al “staging area” con el comando:

    git add nombreDelArchivoModificado.js


 o si escribimos:

    git add .

  Agregamos todos los archivos modificados dentro de working directory a staging area.

 Cuando pasamos el código de Working Directory a Stagin Area, cambiamos el estado del código de modificado a preparado.
  
### Staging Area:

 Es donde le podemos dar nombre a nuestra nueva versión. Y crear una “copia” de cómo quedaría nuestra nuestro repositorio en producción.

 Para pasar nuestro código de staging area al Git Repository (aun no se publica el código en Github o GitLab), escribimos el siguiente comando:

    git commit -m "Nombre del la nueva versión"

 Nota que cuando hacemos el commit el código pasa de estado preparado a confirmado.
 
### Repository

 Una vez que el código esta confirmado ya esta listo para sincronizarse con el servidor de Git (github,gitlab, bitbucket,etc). Para hacer esto, escribimos:

    git push -u origin master

 Como  adicional, -u sirve para recordar “origin” que es la dirección del repositorio de git y “master” que es la branch del repositorio que se esta usando.

 Como estamos “recordando” origin y master, para el próximo pusheo, únicamente podríamos hacer:

    git push

 Y git ya sabe que significa:

    git push origin master

 Si estamos trabajando en un repositorio con varias personas en algún momento vamos a querer descargar las cosas que otros compañeros agreguen o modifiquen.

 Para eso ejecutaremos:

     git pull origin master

 Y git automáticamente agrega los cambios, (y se pasa por el proceso checkout del proyecto, mostrado en el diagrama de arriba).

Este nuevo diagrama, puede ayudar a entender un poco mejor el flujo que acabamos de explicar


![](/imagen/Imagen10.png)

## 5. Describa el flujo para crear un nuevo repositorio git.

    1) Crear un directorio 

    2) Anrrancar el Git Bash

    3) Nos posicionamos en el directorio

    4) Ejecutamos el comando **git init** para crear un repositorio  

    5) Se crea una carpeta .git, es una carpeta oculta que utiliza git

## 6. Describa el flujo para agregar un archivo simple al repositorio.

    1-Crear el archivo

    2-Incorporar el codigo

    3-Grabar el archivo
   
    4- Ejecutar el comando: git add “nombre_de_archivo“

    5- Ejecutar el comando: git commit -m "Nombre del la nueva versión"

## 7. Describa el flujo para cambiar el archivo agregado y guardar los cambios en el repositorio.

    1-Abrir el archivo

    2-Modificar el codigo

    3-Grabar el archivo
   
    4- Ejecutar el comando: git add “nombre_de_archivo“

    5- Ejecutar el comando: git commit -m "Nombre del la nueva versión"


## 8. ¿Cómo hago para ignorar un archivo o carpeta?

    1-Crear un archivo: .gitignore

    2-Abrir el archivo .gitignore, 

    3-Dentro del archivo .gitignore, se debe poner el nombre de los archivos o carpeta que se quiere ignorar.
   
    4- Grabar el archivo .gitgnore: 

    5- Despues de esto el sistema GIT ignora los archivo o carpeta.


## 9. Explique qué es un branch. Dé un pequeño ejemplo de cómo haría uno.

  branch es una nueva rama de trabajo.

  El comando usado para crear  una nueva rama es:

    git branch "nombre de la rama"  

## 10. ¿Qué hago con `git add .`?

 El comando:

    git add . 

  Sirve para agregar varios achivos modificados al staging Area, ante de hacer el commit. 

## 11. ¿Cómo cambiamos de un branch a otro?

Si estás en una rama y pasamos a otra usamos:

    git checkout <nombre>
    


## 12. Investigue qué es Markdown y responda todas las preguntas anteriores en este lenguaje (con el nombre de archivo RESPUESTAS.md). Súbalo al repositorio.

## ¿Que es Markdown? 
Markdown es un lenguaje de marcado que facilita la aplicacion de formato a un texto empleando una serie de caracteres de una forma especial. En principio, fue pensado para elaborar textos cuyo destino iba a ser la web con mas rapidez y sencillez.

## La historia de Markdown

John Gruber crea el lenguaje Markdown en 2004, con una ayuda importante de Aaron Swartz en la sintaxis. pudiera escribir usando un formato de texto plano facil deleer y facil deescribir, y con la posibilidad de poder convertir su documento.

## ¿Para que se usa?
Es una herramienta de software que convierte el lenguaje en HTML valido. Trata de conseguir la maxima legibilidad y facilidad de publicación tanto en su forma de entrada como de salida, impirandoce  en muchas convenciones existentes para marcar mensajes de correo electrónico usando texto plano.

## Algunos ejemplo son 
1.Para escribir en Negrita,  hay que escribir:

     **Esto es negrita**
**Esto es negrita**

2.Para escribir en italica, hay que escribir:  
      
       *cursiva*

*cursiva*

3.Encabezados: 

Nivel 1 

    # Encabezado nivel uno

# Encabezado nivel uno

Nivel 2

    ## Encabezado nivel dos 

## Encabezado nivel dos 

Nivel 3

    ### Encabezado nivel dos 

### Encabezado nivel tres  

4.Enlaces (links)

caso 1:

    Este es un link hacia [la página web de ANSES](https://www.anses.gob.ar/).

Este es un link hacia [la página web de ANSES](https://www.anses.gob.ar/).

Cómo vincular números de teléfonos:

    Este es un link al teléfono [(54–11) 5985-8700 ](tel:+541159858700)

Este es un link al teléfono [(54–11) 5985-8700 ](tel:+541159858700)



5.Listas

Para lista sin numeración usá asterisco, signo de suma o guión corto (*, + o -) como si se tratara de viñetas. Recordá dejar un salto de línea antes de comenzar a enlistar.

     -    Lista de ítems sin numeración.
     -    Lista de ítems sin numeración.

-    Lista de ítems sin numeración.
-    Lista de ítems sin numeración.

Lista con número

    1.  Lista de ítems numerada.
    2.  Lista de ítems numerada
   
1.  Lista de ítems numerada.
2.  Lista de ítems numerada



